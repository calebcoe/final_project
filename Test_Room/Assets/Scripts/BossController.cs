﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossController : MonoBehaviour
{
    //Set slot for health
    public int health;
    //Add slot for prefab explosion animation to be placed in
    public GameObject explosion;
    //Set range at which enemy engages
    public float playerRange = 10f;
    //Add slot for a Rigidbody
    public Rigidbody2D theRB;
    
    //Add slot for the move speed
    public float moveSpeed;

    //Add check box for whether the enemy should even shoot
    public bool shouldShoot;
    //Shoot every half second(very deadly)
    public float fireRate = 0.5f;
    //Keep track of how long to wait between each bullet being fired
    private float bossShotCounter;
    //Add slot for prefab bullet to be placed in
    public GameObject bullet;
    //Add slot for a firepoint location to be placed in
    public Transform firePoint;


    //Add check box for whether the enemy should use a melee attack
    public bool bossMeleeAttack;
    //Strike every second
    public float bossMeleeRate = 1f;
    //Keep track of how long to wait between each strike
    private float bossMeleeCounter;
    //Add slot for melee damage amount. The entered type should be an integer (int)
    public int bossMeleeDamageAmount;

    //Add slot for the melee UI
    public GameObject claw;


    //NOT TOO SURE WHAT THIS GUY DOES
    public GameObject player;


    //Add slot for a firepoint location to be placed in
    public Transform strikePoint;

    //Add slot for second boss phase UI
    public GameObject bossPhaseTwo;


    // Start is called before the first frame update
    void Start()
    {



    }

    // Update is called once per frame
    void Update()
    {
        //Distance will be equal to the 3D distance between the boss and the player
        float dist = Vector3.Distance(transform.position, player.transform.position);
        //If the boss health GREATER THAN 10 do some stuff
        if (health > 10)
        {
            //If the distance is less than the player range (distance to player) do some stuff
            if (dist < playerRange)
            {
                //Damage the player with melee damage
                player.GetComponent<PlayerController>().TakeBossDamage(bossMeleeDamageAmount);
                //Recalculate the position of the player
                Vector3 playerDirection = PlayerController.instance.transform.position - transform.position;
                //Add movement to the boss
                theRB.velocity = playerDirection.normalized * moveSpeed;

                //If the boss does a melee attack
                if (bossMeleeAttack)
                {

                    //Reset the melee timer
                    bossMeleeCounter -= Time.deltaTime;

                    //If the melee timer hits 0 do some stuff
                    if (bossMeleeCounter <= 0)
                    {
                        //Create the claw animation
                        Instantiate(claw, strikePoint.position, strikePoint.rotation);
                        //Set the melee timer to be equal to the melee rate
                        bossMeleeCounter = bossMeleeRate;


                    }

                }
            }
            //If the distance is greater than the player range do some stuff
            else
            {
                //Make the boss stay still
                theRB.velocity = Vector2.zero;
            }
        }
        //If the boss health is LESS THAN 10 do some stuff
        else
        {
            //Set move speed to 0
            moveSpeed = 0;

            //Distance will be checking the distance between two points (enemy position, player position) and seeing if that distance is less than (<) the playerRange
            if (Vector3.Distance(transform.position, PlayerController.instance.transform.position) < playerRange)
            {
                //Set the vector for the players direction
                Vector3 playerDirection = PlayerController.instance.transform.position - transform.position;
                //Reset the movement of the boss
                theRB.velocity = playerDirection.normalized * moveSpeed;

                //If the boss should shoot do some stuff
                if (shouldShoot)
                {
                    //Reset the shoot timer
                    bossShotCounter -= Time.deltaTime;
                    //If the shoot counter hits 0 do some stuff
                    if (bossShotCounter <= 0)
                    {
                        //Create a bullet to be shot at the player
                        Instantiate(bullet, firePoint.position, firePoint.rotation);
                        //Set the timer equal to the fireRate variable
                        bossShotCounter = fireRate;
                    }
                }


            }
            //If the player is out of range do some stuff
            else
            {
                //Make the boss stay still
                theRB.velocity = Vector2.zero;
            }

        }
    }

  

    //Set up function for enemies to take damage
    public void TakeDamage()
    {
        //Each time damage is taken 1 health will be taken away
        health--;

        //If the health drops down to 10 or less do some stuff
        if (health <= 10)
        {
            //Activate the second phase of the boss making them stronger
            bossPhaseTwo.SetActive(true);
            //Set health to 10
            health = 10;
        }
        // ADD THIS CODE TO THE ABOVE FOR LOOP
        if (health <= 10)
        {
            
            //Destroy the current object (Boss)
            Destroy(gameObject);
            //Replace the enemy with the explosion animation
            Instantiate(bossPhaseTwo, transform.position, transform.rotation);
            
        }
        //Check if the enemy is dead
        if (health <= 0)
        {
            //Destroy the current object (Boss)
            Destroy(gameObject);
            //Replace the enemy with the explosion animation
            Instantiate(explosion, transform.position, transform.rotation);

        }


    }
}
