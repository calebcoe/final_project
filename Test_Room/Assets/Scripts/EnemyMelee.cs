﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMelee : MonoBehaviour
{
    //Set slot for health
    public int health = 3;
    //Add slot for prefab explosion animation to be placed in
    public GameObject explosion;
    //Set range at which enemy engages
    public float playerRange;
    //Add slot for a Rigidbody
    public Rigidbody2D theRB;
    //Add slot for the move speed
    public float moveSpeed;


    //Add check box for whether the enemy should use a melee attack
    public bool meleeAttack;
    //Strike every second
    public float meleeRate = 1f;
    //Keep track of how long to wait between each strike
    private float meleeCounter;
    //Add a slot for the melee damage to be set
    public int meleeDamageAmount;
    //Add a slot for the claw UI
    public GameObject claw;
    //Add a slot for the player
    public GameObject player;
    //Add slot for a firepoint location to be placed in
    public Transform strikePoint;


    // Start is called before the first frame update
    void Start()
    {
        


    }

    // Update is called once per frame
    void Update()
    {
        //Distance will be equal to the 3D distance between the enemy and the player
        float dist = Vector3.Distance(transform.position, player.transform.position);
        //Distance will be checking the distance between two points (enemy position, player position) and seeing if that distance is less than (<) the playerRange
        if (dist < playerRange)
        {
            //Damage the player with melee damage
            player.GetComponent<PlayerController>().TakeMeleeDamage(meleeDamageAmount);
            //Recalculate the position of the player
            Vector3 playerDirection = PlayerController.instance.transform.position - transform.position;
            //Add movement to the enemy
            theRB.velocity = playerDirection.normalized * moveSpeed;

            //If the enemy should melee do some stuff
            if (meleeAttack)
            {
                
               //Reset the melee timer
               meleeCounter -= Time.deltaTime;
               //If the melee timer hits 0 do some stuff
               if (meleeCounter <= 0)
               {
                  //Create the claw animation
                  Instantiate(claw, strikePoint.position, strikePoint.rotation);
                  //Set the melee timer to be equal to the melee rate
                  meleeCounter = meleeRate;

                    
                }
            }
        }
        //If the player is out of range do nothing
        else
        {
            theRB.velocity = Vector2.zero;
        }

    }

    //Set up function for enemies to take damage
    public void TakeDamage()
    {
        //Each time damage is taken 1 health will be taken away
        health--;
        //Check if the enemy is dead
        if (health <= 0)
        {
            //Destroy the current object (enemy)
            Destroy(gameObject);
            //Replace the enemy with the explosion animation
            Instantiate(explosion, transform.position, transform.rotation);
        }


    }
}
