﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPunch : MonoBehaviour
{
    //Add slot to set damage amount
    public int meleeDamageAmount;
    //Add slot for punch speed
    public float punchSpeed = 3f;
    //Add slot for RigidBody
    public Rigidbody2D theRB;

    //Set direction variable set to a 3D vector
    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        //Get the players direction by taking their current position(x, y and z) and subtracting the position of the enemy striking
        direction = PlayerController.instance.transform.position - transform.position;
        //Turn the direction binary representation to Unicode
        direction.Normalize();
        //Set the velocity for the punch
        direction = direction * punchSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        //Set velocity for the rigidbody
        theRB.velocity = direction * punchSpeed;
    }

    //When another 2D object enters the collider (circle in this case) do some stuff
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check if the 2D object that entered the collider is the player
        if (other.tag == "Player")
        {
            //Deal damage to the player
            PlayerController.instance.TakeMeleeDamage(meleeDamageAmount);
            //Destroy the claw
            Destroy(gameObject);
        }
    }
}
