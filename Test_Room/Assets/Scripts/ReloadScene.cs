﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Add SceneManagement Unity attachment
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetScene()
    {
        //Reset the game
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
