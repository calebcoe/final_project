﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    //Add slot for the amount of ammo the pickup has
    public int healAmount = 30;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //When another 2D object enters the collider (circle in this case) do some stuff
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check if the 2D object that entered the collider is the player
        if (other.tag == "Player")
        {
            //Add the ammoAmount to the players current ammo
            PlayerController.instance.currentHealth += healAmount;
            PlayerController.instance.AddHealth(healAmount);

            //Destroy the pickup object (ammo)
            Destroy(gameObject);
        }
    }
}
