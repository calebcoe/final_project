﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    //Set slot for health
    public int health = 3;
    //Add slot for prefab explosion animation to be placed in
    public GameObject explosion;
    //Set range at which enemy engages
    public float playerRange = 10f;
    //Add slot for a Rigidbody
    public Rigidbody2D theRB;
    //Add slot for the move speed
    public float moveSpeed;

    //Add check box for whether the enemy should even shoot
    public bool shouldShoot;
    //Shoot every half second(very deadly)
    public float fireRate = 0.5f;
    //Keep track of how long to wait between each bullet being fired
    private float shotCounter;
    //Add slot for prefab bullet to be placed in
    public GameObject bullet;
    //Add slot for a firepoint location to be placed in
    public Transform firePoint;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Distance will be checking the distance between two points (enemy position, player position) and seeing if that distance is less than (<) the playerRange
        if (Vector3.Distance(transform.position, PlayerController.instance.transform.position) < playerRange)
        {

            Vector3 playerDirection = PlayerController.instance.transform.position - transform.position;

            theRB.velocity = playerDirection.normalized * moveSpeed;

            if(shouldShoot)
            {
                shotCounter -= Time.deltaTime;
                if(shotCounter <= 0)
                {
                    Instantiate(bullet, firePoint.position, firePoint.rotation);
                    shotCounter = fireRate;
                }
            }
        } else
        {
            theRB.velocity = Vector2.zero;
        }

    }

    //Set up function for enemies to take damage
    public void TakeDamage()
    {
        //Each time damage is taken 1 health will be taken away
        health--;
        //Check if the enemy is dead
        if(health <= 0)
        {
            //Destroy the current object (enemy)
            Destroy(gameObject);
            //Replace the enemy with the explosion animation
            Instantiate(explosion, transform.position, transform.rotation);
        }


    }
}
