﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    //Add slot to set damage amount
    public int damageAmount;
    //Add slot for bullet speed
    public float bulletSpeed = 3f;
    //Add slot for RigidBody
    public Rigidbody2D theRB;

    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        //Get the players direction by taking their current position(x, y and z) and subtracting the position of the enemy firing the bullet
        direction = PlayerController.instance.transform.position - transform.position;
        direction.Normalize();
        direction = direction * bulletSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        theRB.velocity = direction * bulletSpeed;
    }

    //When another 2D object enters the collider (circle in this case) do some stuff
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Check if the 2D object that entered the collider is the player
        if (other.tag == "Player")
        {
            //Deal damage to the player
            PlayerController.instance.TakeDamage(damageAmount);
            //Destroy the bullet
            Destroy(gameObject);
        }
    }
}
